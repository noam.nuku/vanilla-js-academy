# My Vanilla JS Academy Projects: Winter 2019
#### All Lessons were created and taught by Chris Ferdinandi [https://gomakethings.com/](https://gomakethings.com/).

- [Project 1](https://gitlab.com/noam.nuku/vanilla-js-academy/tree/master/project1)
- [Project 2](https://gitlab.com/noam.nuku/vanilla-js-academy/tree/master/project2)
